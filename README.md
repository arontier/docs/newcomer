# Welcome

Please read *top to bottom*; there are *dependencies in the order* of sections.

[toc]


<!--
## Welcome to the team 😊🎉🥳🍀🤞

![Welcome to Platform Team](welcome.jpg)
*(Left to right) DJ, MK and HC taken on 28 April 2022 at [Bruce Lee](https://www.google.com/maps/place/%EB%B8%8C%EB%A3%A8%EC%8A%A4%EB%A6%AC/data=!3m1!4b1!4m6!3m5!1s0x357ca6b5a706f905:0x481f451991a5e5a2!8m2!3d37.4797395!4d127.0428495!16s%2Fg%2F1tgjzl0p?entry=ttu) by EK*
-->

## Setting up accounts

- [Setting up your computer](setup/host.md)
- [Service accounts](setup/services.md)
- [Server accounts](setup/servers.md)
- [OpenVPN certificates](setup/openvpn.md)

> Note. You should set up a GitLab account with company email address to read the following sections.

### Checklist for accounts

- [x] Company email address (e.g. oliver@arontier.co)
- [x] Slack access with company account
- [ ] An account on your computer (N.B. username: oliver)
- [ ] An easy and memorable name for your computer
- [ ] A static IP address for your computer
- [ ] An SSH key pair on your computer
- [ ] Printer(s) added to your computer
- [ ] GitLab access with company account (N.B. username: arontier-oliver)
- [ ] ClickUp access with company account
- [ ] Accounts on devs (development servers) (N.B. username: oliver)
- [ ] OpenVPN access headquarter network (N.B. username: oliver)

## Short tour on services

- [AD3](services/ad3.md): AI-driven Drug Discovery Development
- [FARM](services/farm.md): Flow Accepting Ranch of Machines
- ~~[OSP](services/osp.md): Osong Protein/Vaccine (formally KBIO Health Protein/Vaccine)~~
- [PATH](services/path.md): PATHology

## Short tour on servers and networks

> *devs* is a term to denote the collection of development servers and *enchanters* for the collection of production servers. You may find yourself coming back here and read these resources from time to time; be familiarized with the resources.

- [devs](https://gitlab.com/arontier/docs/enchanters/-/blob/main/resources/servers.md#devs-development-servers): development servers
- [enchanters](https://gitlab.com/arontier/docs/enchanters/-/blob/main/resources/servers.md#enchanters-production-servers): production servers
- [dev networks](https://gitlab.com/arontier/docs/devs/-/blob/main/resources/networks.md): development networks
- [enchanter networks](https://gitlab.com/arontier/docs/enchanters/-/blob/main/resources/networks.md): production networks

## Tutorials for *immediately* required skills

These skills are used everyday in Platform team.
Feel free to skip them if you *mastered* the topics already. 😊

- Docker, a walk-through: https://docs.docker.com/get-started/
- Docker, a Youtube playlist: https://www.youtube.com/playlist?list=PL4cUxeGkcC9hxjeEtdHFNYMtCpjNBm3h7
- Docker compose, a walk-through: https://docs.docker.com/compose/gettingstarted/
- Docker compose, a Youtube: https://www.youtube.com/watch?v=DM65_JyGxCo
- Git, a walk-through: https://gitimmersion.com/
- Git branching, interactively!: https://learngitbranching.js.org/?locale=ko
- Git flow, an article: https://nvie.com/posts/a-successful-git-branching-model/

A list of articles on frequently used Docker images is [here](https://gitlab.com/arontier/docs/handbook/-/blob/main/tips/docker-images.md).

## Setting up development environment

- Set up multiple identities for GitLab: https://gitlab.com/arontier/docs/handbook/-/blob/main/tips/git-multiple-identities.md
- Set up multiple SSH keys for GitLab: https://gitlab.com/arontier/docs/handbook/-/blob/main/tips/ssh-multiple-keys.md
- Install dnsmasq, a DNS forwarder: https://gitlab.com/arontier/compose/dnsmasq
- Install traefik, a reverse proxy: https://gitlab.com/arontier/compose/traefik
- Do essential things to your Visual Studio Code: https://gitlab.com/arontier/docs/handbook/-/blob/main/tips/vscode-summary.md

### Checklist for installed apps

- [ ] Google Chrome
- [ ] (optional) Slack client (You can use it with web browsers)
- [ ] (optional) ClickUp client (You can use it with web browsers)
- [ ] OpenVPN client
- [ ] Postman client
- [ ] Homebrew (only if your computer is a Mac)
- [ ] Git
- [ ] Docker
- [ ] Visual Studio Code with extensions
- [ ] (optinal) Easy-to-use shell environment for multiple identities for git (company account and personal accounts)
- [ ] A working traefik Docker container
- [ ] A working dnsmasq Docker container


## Exercises for Docker and networks

Since you have Docker, traefik and dnsmaq installed on your computer, let's use them.
Feel free to skip them if you mastered the topics already.

### Using Docker

> Do this step *on* your computer *not* devs.

Get traefik/whoami Docker image and make a container out of this image using `docker container run` command 
so that traefik/whoami [landing page](https://accesto.com/blog/static/7eb6ab185fc62eeab514354aea010c30/9cab2/proxied-service.png)
shows up with address http://localhost:8080 in Chrome. 

Do not search except Docker CLI. Good luck.  🍀

### Using Docker compose, dnsmaq and traefik

> Do this step *on* your computer *not* devs.

We repeat the previous exercise. But this time two containers with two different URLs http://whoami.example and http://whoareyou.example 
one for each using `docker compose up` command.  Note that no port number is used. 
You should make two compose.yaml files one for each to run the command, again one for each. 
To ensure you have two different containers running, their Hostname values must be different in 
[landing page](https://accesto.com/blog/static/7eb6ab185fc62eeab514354aea010c30/9cab2/proxied-service.png).

You can search if you like. Good luck. 🍀

## Getting accustomed to repositories

Please read https://gitlab.com/arontier/docs/handbook carefully and come back here.
To familiarize yourself with development repositories, do the following steps. 

### Running development repositories

> Do this step *on* your computer *not* devs.

Team members are assigned projects (or services). 
Please ask your team leader if you are not sure what projects you are assigned.
Once you have assigned projects, figure out which repositories you should use for development. 
For example if you are a Python backend developer and AD3 is assigned,
you can use the backend repository in AD3 project for development. 
The repository URLs of each project are listed in
[Short tour on services](#short-tour-on-services) above.

By using git, you now *clone all development repositories and run them one by one*
on your computer. Compare your settings with the ones on devs.
And then show what you have done to your team leader. Good luck. 🍀

### Making and running mock-up repositories

> Do this step *on* your computer *not* devs.

The next challenge is to *make and run mock-up repositories of what you run in the previous 
step*. Don't worry, it's not as bad as it sounds. 

By mock-up repository it means everything is the same as the original repository
except source code itself.  In other words you do not write up source code
but use the framework skeleton as source code.
Note that you still need to make everything else:
repository directory structure, git-related files, Docker-related files, and so on.
To do so, please consult [Development repository checklist](https://gitlab.com/arontier/docs/handbook#development-repository-checklist).

While you make and run mock-ups, try not to look at the original repositories.
Once you finish, show what you have done to your team leader.
If you are stuck, don't hesitate to ask any one in the team. Good luck. 🍀

### Learning how to release

Our repositories are updated everyday and frequently
updated version is released to enchanters.
Learning how to release updated software is *important* and *essential* to
all team members; you are expected to be able to do it by yourself in near future. 
You should *ask your team mates* working on the same repositories
*to show you how to do release process for the next release*.

> Note. ClickUp is used for project management. For each project there is a corresponding
> ClickUp Space.  Each Space in turn has release checklist templates used to minimize possible human mistakes.
> These release checklists may give you some idea on how a release process goes.
> 
> We show how to get to AD3 backend release checklist template as an example. 
> But you can find other checklists similarly.
> Open ClickUp and sign in. Find AD3 Space on left panel and click three dots after Space name.
> Select Templates and Browse Templates. For Template Types make sure to uncheck everything 
> and check Checklist only. You will find aid-backend release. Click it to read the checklist template.
>
> Note. Not all release checklists (not template) are the same. The opposite is true:
> they are different all the time.  The checklist template is imported to a release task and 
> the copy of checklist template is updated to suit the release of interest. It becomes a checklist
> for the release task.

### Understanding services

What you have done so far is exercise technical strength, i.e. you can do it without
knowing any service logics. It's time to understand service (or projects) logics:
what they are for, what technology is used, and how they work.

> Note. This step is required by all team members, even if you are not a developer, to understand what services are 
> created by the team.

> Note. Each service consists of a set of applications or apps. Apps in a service do different things 
> but share one common purpose, which defines the identity of the service in hand.
> Do not worry too much how apps work; they are based on scientific subjects of Chemistry, Medicine, Pathology and so on,
> and none of them is our team's topic.

Given service, do the following:

- Sign up using company email address and sign in.
- Ask team members who work on the service for sample inputs for apps.
- Run all apps one by one with corresponding sample input.
- Read output report for each app.
- Figure out the relationship between input and output for each app.

Think each app as a black box and concentrate on the relationship of I/O for the black box.
In other words, remember what each black box is for in plain English and how the input is used to get the output.

Furthermore, if you are a developer, you should have some ideas on service logics by now. A few examples: 

- how authentication is done
- how authorization is done
- what input file formats are used and where to find most used parser
- what output file formats are used and where to find most used renderer
- what HTTP request methods are used

Without reading source code of frontend and backend and using services as an end user, 
it is hard to understand service logics in depth.
It takes time and efforts but you will get there as long as you try.
Good luck. 🍀🍀

### Reading between code lines

This is the hardest and most essential part of developing something.
Even though we try to document basic rules, usage and conventions of
each project&mdash;frontend and backend&mdash;there are many undocumented facts and things
coming from short of packages, technical difficulty caused by service nature, 
choice of framework and/or programming paradigm, experience and so on.
Learning what is written between code lines takes time and 
*is* really *to understand* the service itself.

Whenever you find in code lines what does not make sense to you,
ask your team member working on the same repositories why it is written in that way.
He/she may have an answer. Or may not. If there's no reason to be found, 
you try to *improve the code after discuss with your collegues*.

Of course there are certain things too obvious to everyone except you
(because you just joined the team). You should not waste your time; 
ask tips right away *what they are and why they are*. Sure there are bunch of such 
things for every repository. So *ask tips now*. Good luck. 🍀🍀🍀

## Essential skills for developer

These skills are not required immediately but it *is* essential. 
It takes practice to master them. Be patient and never give up.

- ~~Vim, a tutorial: https://www.openvim.com/tutorial.html~~
- Vim, a practice tool: http://vimgenius.com/
- Vim, a walk-trough: https://www.freecodecamp.org/news/vim-beginners-guide/
- Linux commands, a tutorial: https://www.freecodecamp.org/news/command-line-for-beginners/
- Bash shell scripting, a tutorial: https://www.freecodecamp.org/news/bash-scripting-tutorial-linux-shell-script-and-command-line-for-beginners/
