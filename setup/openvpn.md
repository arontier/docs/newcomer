# OpenVPN certificates

Ask a system administrator to issue OpenVPN certificates for you.

- Instruction: https://github.com/arontier/software/blob/main/README.md#openvpn
- What is VPN: https://www.cisco.com/c/en/us/products/security/vpn-endpoint-security-clients/what-is-vpn.html
- What is OpenVPN: https://openvpn.net/faq/what-is-openvpn/
- App download: https://openvpn.net/client/
