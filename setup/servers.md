# Server accounts

## devs (for development)

Ask the team leader to get your account. Once your account is ready, sign in to `sdev01` to change your default password.
Then place your SSH public key to sdev01 to sign in without password.
Having an account on devs means you also have an account on research servers. Sign in to `deep` to do the same.

- How to change password: https://www.ionos.com/digitalguide/server/configuration/change-linux-password/
- How to change login shell: https://wiki.ubuntu.com/ChangingShells
- dev servers: https://gitlab.com/arontier/docs/enchanters/-/blob/main/resources/servers.md#development-servers
- dev networks: https://gitlab.com/arontier/docs/devs/-/blob/main/resources/networks.md

## enchanters (for production)

No one has a personal account on any enchanter. There are only two users on enchanters for maintenance and deployment.

- enchanter servers: https://gitlab.com/arontier/docs/enchanters/-/blob/main/resources/servers.md#production-servers
- enchanter networks: https://gitlab.com/arontier/docs/enchanters/-/blob/main/resources/networks.md
