# Setting up your computer

In this document, *company account name* denotes the substring before at symbol (`@`) in company email address.
E.g. If your email address is oliver@arontier.com, oliver is your company account name.

[toc]

## Your account name

Please make an account with company account name on your computer.

## Your hostname

Your hostname should be short and valid.
Valid characters for hostnames are ASCII letters from a to z, the digits from 0 to 9, and the hyphen (-).
A hostname may not start with a hyphen.
Please configure your computer name.

- Hostname syntax: https://en.wikipedia.org/wiki/Hostname
- How to change hostname on Mac: https://support.apple.com/guide/mac-help/change-computers-local-hostname-mac-mchlp2322/mac
- How to change hostname on Linux: https://linuxhint.com/change-linux-hostname-without-restart/

## MAC/IP binding

To have a persistent IPv4 address for your computer, 
find out your computer (preferably WiFi) MAC address and give it to the team leader.

- What is MAC/IP binding: https://www.makeuseof.com/what-is-mac-binding-how-does-it-work/
- How to find MAC address: https://kb.mit.edu/confluence/pages/viewpage.action?pageId=4273729
- What is DHCP: https://learn.microsoft.com/en-us/windows-server/networking/technologies/dhcp/dhcp-top

## SSH key authentication

Create your SSH key pair on your computer.

- What is SSH: https://www.cloudflare.com/learning/access-management/what-is-ssh/
- How to create SSH key pair: https://git-scm.com/book/en/v2/Git-on-the-Server-Generating-Your-SSH-Public-Key
- How to use to authenticate: https://www.digitalocean.com/community/tutorials/how-to-configure-ssh-key-based-authentication-on-a-linux-server
- Asymmetric cryptography introduction: https://iludaslab.tistory.com/96
- RSA paper: https://web.archive.org/web/20230127011251/http://people.csail.mit.edu/rivest/Rsapaper.pdf (you don't need to read it)
- AES specification: https://nvlpubs.nist.gov/nistpubs/FIPS/NIST.FIPS.197-upd1.pdf (you don't need to read it)

## Adding printers and scanners

Add the following printers. Then ask the team leader to configure the scanner 
so that you can send what you scan to your company email address.

### Sindoh D430 (printer/scanner)

- Protocol: IPP (Internet Printing Protocol)
- IPv4 address: 192.168.10.200
- Driver: Generic Postscript Printer 10.4 or above
- Capability: duplex printing
- Manuals and manufacturer site:
    - 복사 설명서: https://s3.ap-northeast-2.amazonaws.com/www.sindoh.com/download/upload/downcenter/1711168119615955.pdf
    - 스캐너 설명서: https://s3.ap-northeast-2.amazonaws.com/www.sindoh.com/download/upload/downcenter/1711168121487587.pdf
    - 팩스 설명서: https://s3.ap-northeast-2.amazonaws.com/www.sindoh.com/download/upload/downcenter/1711168121863874.pdf
    - 사용 설명서: https://s3.ap-northeast-2.amazonaws.com/www.sindoh.com/download/upload/downcenter/1708070731752683.pdf
    - User guide: https://s3.ap-northeast-2.amazonaws.com/www.sindoh.com/download/upload/downcenter/1805164594104241.pdf
    - https://www.sindoh.com/ko/customer/total-search-result.php?key_word=D430

## Essential apps

Install the following essentials.

- Google Chrome: https://www.google.com/chrome/
- Google authenticator (on the phone, only if you don't have any authenticator)
    - Android app: https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2&hl=en&pli=1
    - iOS app: https://apps.apple.com/us/app/google-authenticator/id388497605
- Git: https://git-scm.com/book/en/v2/Getting-Started-Installing-Git
- Homebrew: https://brew.sh/ (only if your computer is a Mac.)
- Visual Studio Code: https://code.visualstudio.com/download
- Docker Desktop: https://www.docker.com/products/docker-desktop/
- Postman: https://www.postman.com/downloads/
- ClickUp: https://clickup.com/download
- Figma: https://www.figma.com/downloads/


## References

- Docker manuals: https://docs.docker.com/manuals/
- Dockerfile reference: https://docs.docker.com/reference/dockerfile/
- Docker CLI reference: https://docs.docker.com/reference/cli/docker/
- Docker compose CLI reference: https://docs.docker.com/compose/reference/
- Postman tutorial: https://learning.postman.com/docs/introduction/overview/