# Put your custom themes in this folder.

function symbol() {
  local symbol=$(git_super_status)
  [[ -z $symbol ]] && symbol="%{$fg_bold[red]%};)%{$reset_color%}" || true
  echo "$symbol"
}

PROMPT='$(virtualenv_prompt_info)'
PROMPT+="%{$fg[yellow]%}%n%{$reset_color%}@%{$fg[blue]%}%m%{$reset_color%}:%{$fg[cyan]%}%~%{$reset_color%}"
PROMPT+=$'\n''$(symbol) '
RPROMPT="%{$fg_bold[red]%}"'%(?..[%?] )'"%{$reset_color%}"'$(date "+%T")'

ZSH_THEME_VIRTUALENV_PREFIX="%{$fg[red]%}"
ZSH_THEME_VIRTUALENV_SUFFIX="%{$reset_color%}:"

ZSH_THEME_GIT_PROMPT_PREFIX="("
ZSH_THEME_GIT_PROMPT_SUFFIX=")"
ZSH_THEME_GIT_PROMPT_SEPARATOR="|"
ZSH_THEME_GIT_PROMPT_BRANCH="%{$fg[red]%}"            # the branch name you are on
ZSH_THEME_GIT_PROMPT_STAGED="%{$fg[yellow]%}%{●%G%}"  # the number of staged files/directories
ZSH_THEME_GIT_PROMPT_CONFLICTS="%{$fg[red]%}%{✗%G%}"  # the number of files in conflict
ZSH_THEME_GIT_PROMPT_CHANGED="%{$fg[blue]%}%{+%G%}"   # the number of changed files
ZSH_THEME_GIT_PROMPT_BEHIND="%{↓%G%}"                 # the number of commits behind
ZSH_THEME_GIT_PROMPT_AHEAD="%{↑%G%}"                  # the number of commits ahead
ZSH_THEME_GIT_PROMPT_UNTRACKED="%{…%G%}"              # the number of untracked files/dirs
ZSH_THEME_GIT_PROMPT_STASHED="%{$fg[yellow]%}%{⚑%G%}" # the number of stashed files/dir
ZSH_THEME_GIT_PROMPT_CLEAN="%{$fg[cyan]%}%{✓%G%}"     # a colored flag indicating a "clean" repo
