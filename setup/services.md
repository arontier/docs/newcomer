# Service accounts

In this document, *company account name* denotes the substring before at symbol (`@`) in company email address.
E.g. If your email address is oliver@arontier.com, oliver is your company account name.

## GitLab

Ask the team leader to invite you to the team's repository group. 
You should use company account name prefixed by `arontier-` for your GitLab user name or display name.
E.g. If your company account name is oliver, use "arontier-oliver" as your user name or display name.

- Website: https://gitlab.com/arontier
- Getting started: https://docs.gitlab.com/ee/tutorials/
- Documentation: https://docs.gitlab.com
- How to set up 2FA: https://docs.gitlab.com/ee/user/profile/account/two_factor_authentication.html
- How to register SSH public key: https://docs.gitlab.com/ee/user/ssh.html

## ClickUp

Ask the team leader to invite you to the team's workspace. 
You should use comany account name prefixed by `arontier-` for your ClickUp user name or display name.
E.g. If your company account name is oliver, use "arontier-oliver" as your user name or display name.

- Website: https://app.clickup.com/31166608 
- Getting started: https://help.clickup.com/hc/en-us/categories/6132419928087-Getting-started
- Documentation: https://help.clickup.com/hc/en-us
- App download: https://clickup.com/download

## GitHub (optional)

Ask a system administrator to invite you to the company's repository organization. 
You should use company account name prefixed by `arontier-` for your GitHub user name or display name.
E.g. If your company account name is oliver, use "arontier-oliver" as your user name or display name.

- Website: https://github.com/arontier
- Getting started: https://docs.github.com/en/get-started 
- Documentation: https://docs.github.com/en
- How to register SSH public key: https://docs.github.com/en/authentication/connecting-to-github-with-ssh/adding-a-new-ssh-key-to-your-github-account
- How to set up 2FA: https://docs.github.com/en/authentication/securing-your-account-with-two-factor-authentication-2fa/configuring-two-factor-authentication

## Further reading

- git: https://git-scm.com/book/en/v2
- GitLab: https://docs.gitlab.com
- GitHub: https://docs.github.com/en
