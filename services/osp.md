# OSP

OSP stands for Osong Protein/Vaccine (formally KBIO Health Protein/Vaccine).

- Slack (OSP): https://arontier.slack.com/archives/C057FEEFDNF
- Slack (OSV): https://arontier.slack.com/archives/C057U7KDAF3
- ClickUp: https://app.clickup.com/31166608/v/s/55152595
- GitLab group: https://gitlab.com/arontier/osp

## backend

- Development server (mdev02): http://backend.osp.arontier.co.kr/admin
- Production server (mage02): https://rest.kbiop.arontier.co/admin
- Git repository: https://gitlab.com/arontier/osp/backend
- Docker registry: https://gitlab.com/arontier/osp/backend/container_registry

## frontend

- Development server (mdev02): http://frontend.osp.arontier.co.kr
- Production server (mage02): https://kbiop.arontier.co
- Git repository: https://gitlab.com/arontier/osp/frontend
- Docker registry: https://gitlab.com/arontier/osp/frontend/container_registry
