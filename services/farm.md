# FARM

FARM stands for Flow Accepting Ranch of Machines.

- ClickUp: https://app.clickup.com/31166608/v/s/55141765
- GitLab group: https://gitlab.com/arontier/farm

## farmer

- Development server (sdev01): http://farmer.farm.arontier.co.kr/admin
- Production server (seer01): https://farmer.arontier.co/admin
- Git repository: https://gitlab.com/arontier/farm/farmer
- Docker registry: https://gitlab.com/arontier/farm/farmer/container_registry

## oliver

- Development server (sdev01): http://cromwell.farm.arontier.co.kr/admin
- Production server (seer01): https://cromwell.arontier.co/admin
- Git repository: https://gitlab.com/arontier/farm/oliver
- Docker registry: https://gitlab.com/arontier/farm/oliver/container_registry

## cromwell

- Development server (sdev01): http://cromwell.farm.arontier.co.kr
- Production server (seer01): https://cromwell.arontier.co
- Git repository (*external*): https://github.com/broadinstitute/cromwell
- Docker registry: https://gitlab.com/arontier/farm/cromwell/container_registry
