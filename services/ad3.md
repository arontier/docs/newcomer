# AD3

## Current version (beaver)

- Slack: https://arontier.slack.com/archives/CF7M2B2MC
- ClickUp: https://app.clickup.com/31166608/v/s/90182526253
- GitLab group: https://gitlab.com/arontier/ad3.beaver

### backend

- Devlopment server (mdev02): http://backend.ad3.arontier.co.kr/admin
- Production server (mage03): https://rest.ad3.io
- Git repository: https://gitlab.com/arontier/ad3.beaver/backend
- Docker registry: https://gitlab.com/arontier/ad3.beaver/backend/container_registry

### frontend

- Devlopment server (mdev02): http://frontend.ad3.arontier.co.kr
- Production server (mage03): https://ad3.io
- Git repository: https://gitlab.com/arontier/ad3.beaver/frontend
- Docker registry: https://gitlab.com/arontier/ad3.beaver/frontend/container_registry

## Previous versions

### alpaca

- Slack: https://arontier.slack.com/archives/CF7M2B2MC
- ClickUp: https://app.clickup.com/31166608/v/s/55146185
- GitLab group: https://gitlab.com/arontier/ad3

#### backend

- Devlopment server (mdev01): http://backend.alpaca.ad3.arontier.co.kr/admin
- Git repository: https://gitlab.com/arontier/ad3/aid-backend
- Docker registry: https://gitlab.com/arontier/ad3/aid-backend/container_registry

#### frontend

- Devlopment server (mdev01): http://frontend.alpaca.ad3.arontier.co.kr
- Git repository: https://gitlab.com/arontier/ad3/aid-frontend
- Docker registry: https://gitlab.com/arontier/ad3/aid-frontend/container_registry