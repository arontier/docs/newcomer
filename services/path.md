# PATH

PATH stands for PATHology.

- Slack: https://arontier.slack.com/archives/C04B71URKGC
- ClickUp: https://app.clickup.com/31166608/v/s/55152533
- GitLab group: https://gitlab.com/arontier/path

## backend

- Development server (mdev02): http://backend.path.arontier.co.kr/admin
- Production server (mage02): https://rest.path.arontier.co/admin
- Git repository: https://gitlab.com/arontier/path/backend
- Docker registry: https://gitlab.com/arontier/path/backend/container_registry

## frontend

- Development server (mdev02): http://frontend.path.arontier.co.kr/admin
- Production server (mage02): https://path.arontier.co
- Git repository: https://gitlab.com/arontier/path/frontend
- Docker registry: https://gitlab.com/arontier/path/frontend/container_registry
